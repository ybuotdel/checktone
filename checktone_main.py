#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2017 Yorick Buot de l'Epine

#
# Cuttone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.
#
# Cuttone is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

import wavio
import numpy 
import os.path
import os
os.environ['ETS_TOOLKIT'] = 'qt5'
#import sip
#sip.setapi('QString', 2)
os.environ['QT_API'] = 'pyqt'
####################################
from PyQt5 import QtGui,QtCore
from PyQt5.QtCore import QObject, pyqtSignal
import pyqtgraph as pg
import pyaudio
import checktone_gui
import param_inverse_gui
#import splashscreen
#import scipy.io.wavfile
import wave
#import sys
import psutil, os

def kill_proc_tree(pid, including_parent=True):    
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()


class plot_sig:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground((235,235,235))
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.courbe=self.p.plot()
        self.courbe.setData(numpy.linspace(0,50,50),numpy.zeros((50)),pen=pg.mkPen('b',width=2))
        self.p.setXRange(0,50)
        self.p.setYRange(-1.1,1.1)
        #mise en forme axe#######
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (V)')
        self.p.setLabel('bottom',text ='Temps (s)')
        #ajout des lignes de visualisation 
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen('y',width=1.5))
        self.vLine2 = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen('y',width=1.5))
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.vLine2, ignoreBounds=True)
        self.reset()
    def reset(self):
#        x_min=(self.p.getViewBox().viewRange()[0][0])
        x_max=(self.p.getViewBox().viewRange()[0][1])
        self.vLine.setValue(0.1*(x_max))
        self.vLine2.setValue(0.9*(x_max))
        self.update_cross()
    def update_cross(self):
        x_min=0.98*(self.p.getViewBox().viewRange()[0][0])
        x_max=0.98*(self.p.getViewBox().viewRange()[0][1])
        x=self.vLine.value()
        x2=self.vLine2.value()
        if x<x_min:
            self.vLine.setValue(x_min)
        if x2>x_max:
            self.vLine2.setValue(x_max)  
        if x2<x:
            self.vLine2.setValue(x+0.05*x_max)
        if x>x2:
            self.vLine.setValue(x-0.05*x_max)
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen(color='b',width=2),autoDownsample=True)        
         self.p.setXRange(0,X[-1])
         self.p.setYRange(numpy.amin(Y),numpy.amax(Y))
         
    def suppr(self):
        self.p.clear()

class plot_2D_freq:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
#        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
        self.courbe=self.p.plot()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.setYRange(-120, 0)
        self.p.setXRange(0, 1)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='<font size="4">Amplitude </font>', units ='<font size="4">dB </font>')
        self.p.setLabel('bottom',text ='<font size="4">Frequency </font>', units ='<font size="4">Hertz </font>')
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.vtext=pg.TextItem() 
        self.htext=pg.TextItem() 
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        self.p.addItem(self.htext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.htext.setFont(font)
        self.vtext.setFont(font)
        self.update_cross()
    def reset(self):
        self.vLine.setValue(0)
        self.hLine.setValue(0)
        self.update_cross()
    def update_cross(self):
        x_max=0.9*(self.p.getViewBox().viewRange()[0][1])
        y_max=0.9*(self.p.getViewBox().viewRange()[1][0])
        x=self.vLine.value()
        y=self.hLine.value()
        ind_x=x
        ind_y=y
        if ind_x<0:
            ind_x=0
            self.vLine.setValue(ind_x)
        if ind_y>0:
            ind_y=0
            self.hLine.setValue(ind_y)
        if ind_x>x_max:
            ind_x=x_max
            self.vLine.setValue(ind_x)
        if ind_y<y_max:
            ind_y=y_max
            self.hLine.setValue(ind_y)        
        self.vtext.setText(text="{0:.1f}".format(ind_x),color=(0,100,0))
        self.htext.setText(text="{0:.0f}".format(ind_y),color=(0,100,0))
        self.vtext.setPos(ind_x,0)
        self.htext.setPos(0,ind_y+20)
               
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen([25,12,112],width=2),autoDownsample=True)        
         self.p.setXRange(0,X[-1])
         
class plot_2D_time:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
#        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
        self.courbe=self.p.plot()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setYRange(-120, 0)
        self.p.setXRange(0,1)
        self.p.setLabel('left', text ='<font size="4">Amplitude </font>', units ='<font size="4">dB </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time</font>', units ='<font size="4">Seconds</font>')
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.vtext=pg.TextItem() 
        self.htext=pg.TextItem()
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        self.p.addItem(self.htext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.htext.setFont(font)
        self.vtext.setFont(font)
        self.update_cross()
    def reset(self):
        self.vLine.setValue(0)
        self.hLine.setValue(0)
        self.update_cross()
    def update_cross(self):
        x_max=0.9*(self.p.getViewBox().viewRange()[0][1])
        y_max=0.9*(self.p.getViewBox().viewRange()[1][0])
        x=self.vLine.value()
        y=self.hLine.value()
        ind_x=x
        ind_y=y
        if ind_x<0:
            ind_x=0
            self.vLine.setValue(ind_x)
        if ind_y>0:
            ind_y=0
            self.hLine.setValue(ind_y)
        if ind_x>x_max:
            ind_x=x_max
            self.vLine.setValue(ind_x)
        if ind_y<y_max:
            ind_y=y_max
            self.hLine.setValue(ind_y)        
        self.vtext.setText(text="{0:.1f}".format(ind_x),color=(0,100,0))
        self.htext.setText(text="{0:.0f}".format(ind_y),color=(0,100,0))
        self.vtext.setPos(ind_x,0)
        self.htext.setPos(0,ind_y+20)
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen([138,0,0],width=2),autoDownsample=True)        
         self.p.setXRange(0,X[-1])

class plot_spectrum:
    def __init__(self,p,var):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
#        colortable=color_table()
        colortable=cubehelix()
        self.image=pg.ImageItem(setAutoDownsample=True)
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        Xaxis.enableAutoSIPrefix(False)
        Xaxis.enableAutoSIPrefix(False)
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*numpy.size(var.data_freq)/ntick))  
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*numpy.size(var.data_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        self.p.setLabel('left', text ='<font size="4">Frequency </font>', units ='<font size="4">Hz </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time </font>', units ='<font size="4">Seconds </font>')     
        ##########################- define cross ###################
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen([25,12,112],width=2))
        self.vLine.setHoverPen(pg.mkPen('w',width=2))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen([138,0,0],width=2))
        self.hLine.setHoverPen(pg.mkPen('w',width=2))
        self.vtext=pg.TextItem() 
        self.htext=pg.TextItem()
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        self.p.addItem(self.htext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.htext.setFont(font)
        self.vtext.setFont(font)
        self.update_cross_by_label(var)
        
    def reset(self,var):
        self.p.removeItem(self.image)
        self.p.removeItem(self.p.getPlotItem().getAxis('bottom'))
        self.p.removeItem(self.p.getPlotItem().getAxis('left'))
#        colortable=color_table()
        colortable=cubehelix()
        self.image=pg.ImageItem(setAutoDownsample=True)
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*numpy.size(var.data_freq)/ntick))  
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*numpy.size(var.data_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        ##########################- define cross ###################
        self.image.setZValue(-1) #◙remet l'image à l'arrière plan
        self.update_cross_by_label(var)
    
    def update_cross(self,var):
        x=self.vLine.value()
        y=self.hLine.value()
        ind_x=int(round(x))
        ind_y=int(round(y))
        if ind_x<0:
            ind_x=0
            self.vLine.setValue(ind_x)
        if ind_y<0:
            ind_y=0
            self.hLine.setValue(ind_y)
        if ind_x>numpy.size(var.data_time):
            ind_x=numpy.size(var.data_time)-10
            self.vLine.setValue(ind_x)
        if ind_y>numpy.size(var.data_freq):
            ind_y=numpy.size(var.data_freq)-2
            self.hLine.setValue(ind_y)
        slide_time_ind=ind_x
        slide_time=var.data_time[ind_x]
        self.vtext.setText(text="{0:.1f}".format(var.slide_time),color=[25,12,112])
        self.vtext.setPos(ind_x,2)
        slide_freq_ind=ind_y
        slide_freq=var.data_freq[ind_y]
        self.htext.setText(text="{0:.0f}".format(var.slide_freq),color=[138,0,0])
        self.htext.setPos(numpy.size(var.data_time)-20,var.slide_freq_ind)
        return (slide_time,slide_time_ind,slide_freq,slide_freq_ind)
    
    def update_cross_by_label(self,var):
        self.vLine.setValue(var.slide_time_ind)
        self.hLine.setValue(var.slide_freq_ind)          
        self.vtext.setText(text="{0:.1f}".format(var.slide_time),color=[25,12,112])
        self.htext.setText(text="{0:.0f}".format(var.slide_freq),color=[138,0,0])
        self.vtext.setPos(var.slide_time_ind,2)
        self.htext.setPos(numpy.size(var.data_time)-20,var.slide_freq_ind)
        
    def update_data(self,mat,db_min,db_max):          
         self.image.setImage(mat,autoDownSample=True)  
         self.image.setLevels([db_min,db_max])       


class Variable:
    def __init__(self):
        self.freq_max = [] #max frequency to study
        self.buffer_size = [] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] #covering for windowed
        self.channel=[] # voie
        self.time_max=[]      
        self.data_time=[]
        self.data_freq=[]
        self.nb_cover=[]
        self.slide_freq=0
        self.slide_freq_ind=0
        self.slide_time=0
        self.slide_time_ind=0        
        self.db_min=[]
        self.db_max=[]
        self.factor_rate=2.0
        self.signal=[]
        self.signal_time=[]
        self.signal_rate=[]
        self.sensi=[]
        self.ref=[]

class signal_BUFF:
    def init(self,signal_size):
        self.data=numpy.zeros((signal_size))
    def add(self,x):
        self.data[0:numpy.size(x)]=x
        self.data=numpy.roll(self.data,-numpy.size(x))

class spectro_BUFF:
    def init(self,spectro_size):
        self.data=1e-7*numpy.ones((spectro_size),'complex')
    def add(self,x,n):
        self.data[:,0:n]=x
        self.data=numpy.roll(self.data,-n)
        self.data[-1,:]=1e-7
    def abs_data(self):
        return numpy.abs(self.data)

def fenetre(weight_hanning,bloc):
    bloc_out=numpy.multiply(weight_hanning,bloc)
    return bloc_out    
    
    
    ############Î definie la table des couleurs
def cubehelix(gamma=1, s=0.5, r=-1.5, h=1.0):
    def get_color_function(p0, p1):
        def color(x):
            xg = x ** gamma
            a = h * xg * (1 - xg) / 2
            phi = 2 * numpy.pi * (s / 3 + r * x)
            return xg + a * (p0 * numpy.cos(phi) + p1 * numpy.sin(phi))
        return color
    array = numpy.empty((256, 3))
    abytes = numpy.arange(0, 1, 1/256.)
    array[:, 0] = get_color_function(-0.14861, 1.78277)(abytes) * 255
    array[:, 1] = get_color_function(-0.29227, -0.90649)(abytes) * 255
    array[:, 2] = get_color_function(1.97294, 0.0)(abytes) * 255
    return array

#def rainbow():
#    lut = numpy.empty((256, 3))
#    abytes = numpy.arange(0, 1, 0.00390625)
#    lut[:, 0] = numpy.abs(2 * abytes - 0.5) * 255
#    lut[:, 1] = numpy.sin(abytes * numpy.pi) * 255
#    lut[:, 2] = numpy.cos(abytes * numpy.pi / 2) * 255
#    return lut
#    
#def color_table():
##    pos = numpy.array([0.0, 0.5, 1.0])
#    pos = numpy.array([0.0, 1.0])
##    color = numpy.array([[0,0,0,255], [87,108,128,255], [175,215,255,255]], dtype=numpy.ubyte)
#    color = numpy.array([[0,0,0,255],[0,255,255,255]], dtype=numpy.ubyte)
#    map = pg.ColorMap(pos, color)
#    lut = map.getLookupTable(0.0, 1.0, 256)
#    return lut

def colormap_gui(fig,min_val,max_val):
    pen = pg.mkPen(color='k', width=2)
    nb_color=255
    value=numpy.array([numpy.linspace(min_val,max_val,nb_color),numpy.linspace(min_val,max_val,nb_color)])
    colortable=cubehelix()
#    colortable=rainbow()
   # colortable=color_table()
    fig.clear()       
    fig.setBackground((235.0,235.0,235.0))
    fig.plotItem.getAxis('left').setPen(pen)
    fig.plotItem.getAxis('bottom').setPen(pen)
    image=pg.ImageItem()
    item_color=fig.getPlotItem()
    image.setLookupTable(colortable)
    fig.addItem(image)
    image.setImage(value,autoDownSample=True) 
    image.setLevels([min_val,max_val])
    Xaxis=item_color.getAxis('bottom')
    Yaxis=item_color.getAxis('left')
    ntick=7
    Ytick_loc=[]
    Ytick_string=[]
    for ii in range(0,ntick+1):
        Ytick_string.append("{0:.1f}".format(round(min_val+(ii*((max_val-min_val)/float(ntick))))))
        Ytick_loc.append(round((ii*((nb_color)/float(ntick)))))
    Ytick=zip(Ytick_loc,Ytick_string)
    Yaxis.setTicks([list(Ytick)])
    Xaxis.hide()
    
def write_spectro(fid,data,freq,time):
    shape=numpy.shape(data)
    for ii in range(0,shape[1]):
        fid.write("{0:.3f}".format(time[ii])+';')
    fid.write('\n')
    for jj in range(0,shape[0]):
        fid.write("{0:.3f}".format(freq[jj])+';')
        for ii in range(0,shape[1]):
            fid.write("{0:.3f}".format(data[jj,ii])+';')
        fid.write('\n')
#    fid.close()
    
class splashScreen:
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash_checktone.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint |
                            QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix))                     # 10 = Windowrole
        self.splash.setPalette(palette)
        
    def show_splash(self):
        self.splash.show()
    def close_splash(self):
        self.splash.close()
           
        
        
class param_window(QtGui.QWidget,param_inverse_gui.Ui_param):
    def __init__(self, parent=None):
        super(param_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Parameter")  
        

class spectro(QtGui.QMainWindow,checktone_gui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(spectro, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"CheckTone") 
        ############### definie la fenetre des paramètre ########
        self.param = param_window()
        ############# initalise variable ###################
        os.chdir('C:\\')
        self.acqui=0
        self.variable=Variable()
        self.voie_box.addItems(['Left','Right'])
        self.voie_box.setCurrentIndex(0)
        self.define_channel()
        self.param.freq_edit.insert('10000')
        self.variable.freq_max = int(self.param.freq_edit.text()) #max frequency to study
        self.param.line_box.addItems(['100','200','400','800','1600','3200'])
        self.param.line_box.setCurrentIndex(2)
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.param.fenetre_box.addItems(['Uniform','Hanning'])
        self.param.fenetre_box.setCurrentIndex(1)
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.param.recouvrement_edit.insert('0.66')
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.param.sensi_edit.insert('1000')
        self.variable.sensi=1e-3*float(self.param.sensi_edit.text())
        self.param.ref_edit.insert('1')
        self.variable.ref=float(self.param.ref_edit.text())
        #####################################################♠
        self.define_delta_f() # définie le delta f
        self.rate_edit.clear()
        self.rate_edit.setEnabled(False)
        #############################################
        self.min_db_edit.insert('-80')
        self.max_db_edit.insert('0')
        ############# inti varibale for initiale plot #############################
        self.define_colormap()
        self.signal_plot=plot_sig(self.plot_signal)
        self.define_data_freq_time()
        self.freq_fft_plot=plot_2D_freq(self.plot_freq_fft)
        self.time_fft_plot=plot_2D_time(self.plot_time_fft)
        self.spectro_plot=plot_spectrum(self.plot_spectro,self.variable)
        ###################################
        self.start_button.setEnabled(False)
        self.listen_button.setEnabled(False)
        self.export_button.setEnabled(False)
        self.wave_button.setEnabled(False)
        #################################################
        self.freq_slide_edit.insert("{0:.1f}".format(0))
        self.time_slide_edit.insert("{0:.1f}".format(0))
        self.select_freq_slide()
        self.select_time_slide()
######################################################################################
        self.connectActions()
         
    def connectActions(self):
        ######################################
        self.param.enregistrement_button.clicked.connect(self.save_param)
        self.param.freq_edit.editingFinished.connect(self.define_delta_f)
        self.param.line_box.currentIndexChanged.connect(self.define_delta_f)
        self.param.fenetre_box.currentIndexChanged.connect(self.define_window)
        #######################################
        self.import_button.clicked.connect(self.import_wave)
        self.option_button.clicked.connect(self.param_open)
        self.start_button.clicked.connect(self.main)
        self.listen_button.clicked.connect(self.listen_sig)
        self.wave_button.clicked.connect(self.save_wave)
        self.voie_box.currentIndexChanged.connect(self.define_channel)
        self.max_db_edit.editingFinished.connect(self.define_colormap)
        self.min_db_edit.editingFinished.connect(self.define_colormap)
        self.export_button.clicked.connect(self.export_spectro_FFT)
        self.export_time_button.clicked.connect(self.export_time_FFT)
        self.export_freq_button.clicked.connect(self.export_freq_FFT)
        self.freq_slide_edit.editingFinished.connect(self.select_freq_slide)
        self.time_slide_edit.editingFinished.connect(self.select_time_slide)
        self.signal_plot.vLine.sigDragged.connect(lambda: self.signal_plot.update_cross())
        self.signal_plot.vLine2.sigDragged.connect(lambda: self.signal_plot.update_cross())
        ###########################################################################################
        self.freq_fft_plot.hLine.sigDragged.connect(self.freq_fft_plot.update_cross)
        self.freq_fft_plot.vLine.sigDragged.connect(self.freq_fft_plot.update_cross)
        self.time_fft_plot.hLine.sigDragged.connect(self.time_fft_plot.update_cross)
        self.time_fft_plot.vLine.sigDragged.connect(self.time_fft_plot.update_cross)
        self.spectro_plot.hLine.sigDragged.connect(self.update_cross_spectro)
        self.spectro_plot.vLine.sigDragged.connect(self.update_cross_spectro)


    def param_open(self):
        """Lance la deuxième fenêtre"""
        self.param.show() 
        
    def import_wave(self):
        filenom=QtGui.QFileDialog.getOpenFileName(self,filter ='wav (*.wav *.)')
        filename=filenom[0]
        try:
            temp=wave.open(filename,'rb')
            time=numpy.linspace(0,float(temp.getnframes())/temp.getframerate(),temp.getnframes())
            self.variable.signal_time=time
            self.variable.signal_rate=float(temp.getnframes())/self.variable.signal_time[-1]
            self.variable.signal=numpy.zeros((temp.getnframes(),2))
            #################################
            sig=wavio.read(filename)
            nb_bit=[8,16,24,38]
            bit=nb_bit[sig.sampwidth-1]            
            if temp.getnchannels()==1:
                #on sauvegarde le signal sur les deux voies
                temp_sig=(sig.data[:,0]/float(2**(bit-1)))
                delta=0
                if numpy.amax(temp_sig)>1:
                    delta=1
                self.variable.signal[:,0]=temp_sig-delta
                self.variable.signal[:,1]=temp_sig-delta
            if temp.getnchannels()==2:
                temp_sig=(sig.data[:,0]/float(2**(bit-1)))
                delta=0
                if numpy.amax(temp_sig)>1:
                    delta=1
                self.variable.signal[:,0]=temp_sig-delta
                temp_sig=(sig.data[:,1]/float(2**(bit-1)))
                self.variable.signal[:,1]=temp_sig-delta
            self.signal_plot.update_data(self.variable.signal_time,self.variable.signal[:,self.variable.channel])
            self.signal_plot.reset()
            self.rate_edit.clear()
            self.rate_edit.insert("{0:.1f}".format(self.variable.signal_rate))
            ##################### on active les bouton #############
            self.start_button.setEnabled(True)
        except:
            filename=QtGui.QFileDialog.getOpenFileName(self,filter ='wav (*.wav *.)')      
      
    def save_param(self):
        self.define_channel()
        self.variable.freq_max = int(self.param.freq_edit.text()) #max frequency to study
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.define_data_freq_time()
        self.variable.sensi=1e-3*float(self.param.sensi_edit.text())
        self.variable.ref=float(self.param.ref_edit.text())
        ########♣ remet les slide au milieu des plage de frequence et de temps
        self.freq_slide_edit.clear()
        self.time_slide_edit.clear()
        self.freq_slide_edit.insert("{0:.1f}".format(self.variable.freq_max/2.0))
        self.time_slide_edit.insert("{0:.1f}".format(0))
        self.select_freq_slide()
        self.select_time_slide() 
        self.param.close()
        
    def define_data_freq_time(self):
        delta_f=self.variable.delta_freq# equivalant au nombre de buffer à remplir par seconde
        self.variable.nb_cover=int(round(1/(1-self.variable.covering)))
        self.variable.data_freq=numpy.arange(0,self.variable.freq_max,delta_f)#[0:-1]
        t1=self.signal_plot.vLine.value()
        t2=self.signal_plot.vLine2.value()
        self.variable.time_max=numpy.abs(t2-t1)
        self.variable.data_time=numpy.linspace(0,self.variable.time_max,
                                               int(round(self.variable.nb_cover*self.variable.time_max*delta_f)))
        ################## créer les buffers ##############################################
        ###### Signal audio pour toute la duree de l'enregistrement #####
        self.signal_bufferG=signal_BUFF()
        self.signal_bufferG.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        self.signal_bufferD=signal_BUFF()
        self.signal_bufferD.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        ###################### spectre sur toute la durrée de l'enregistrement + nouveau spectre generé ###########
        self.spectro_buffG=spectro_BUFF()
        self.spectro_buffG.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 
        self.spectro_buffD=spectro_BUFF()
        self.spectro_buffD.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 



    def define_channel(self):
        voie=self.voie_box.currentText()
        if voie=='Left':
            self.variable.channel=0
        if voie=='Right':
            self.variable.channel=1
        try:
            self.signal_plot.update_data(self.variable.signal_time,self.variable.signal[:,self.variable.channel])
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec le logarithm
            else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec 
            self.spectro_plot_ini.update_data(20*numpy.log10(data_plot.T),self.variable.db_min,self.variable.db_max)
        except:
            pass
            
        
    
    def define_window(self):
        fenetre=self.param.fenetre_box.currentText()
        if fenetre=='Uniform':
            self.param.recouvrement_edit.setText('0')
        if fenetre=='Hanning':
            self.param.recouvrement_edit.setText('0.66')
               
    def define_delta_f(self):
        self.param.enregistrement_button.setEnabled(True)
        freq_max=int(self.param.freq_edit.text())
        ####################################################################
#        if freq_max>=500 and freq_max<=9000:
        buffer =self.variable.factor_rate*int(self.param.line_box.currentText()) # size of buffer
        self.param.enregistrement_button.setEnabled(True)
        self.variable.delta_freq=int(self.variable.factor_rate*freq_max)/float(buffer)
        self.param.delta_label.setText("{0:.3f}".format(self.variable.delta_freq))

    
    def define_colormap(self):
        val_min=int(self.min_db_edit.text())
        val_max=int(self.max_db_edit.text())
        colormap_gui(self.plot_colorbar,val_min,val_max)
        self.variable.db_min=val_min
        self.variable.db_max=val_max
        ############# si pas d'acquisition, on replote le spectre du buffer #####
        if self.acqui==1:
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)/float(self.variable.delta_freq)
            else:
                data_plot=abs(self.spectro_buffD.data)/float(self.variable.delta_freq)
            self.spectro_plot.update_data(20*numpy.log10(data_plot.T/float(self.variable.ref)),self.variable.db_min,self.variable.db_max)
############################################################################################    
        
    def select_freq_slide(self):
        freq_search=float(self.freq_slide_edit.text())
        freq_list=abs(self.variable.data_freq-numpy.array([freq_search]*len(self.variable.data_freq)))
        ind=numpy.where(freq_list==min(freq_list))[0]
        self.variable.slide_freq_ind=ind[0]
        self.variable.slide_freq=self.variable.data_freq[self.variable.slide_freq_ind]
        self.spectro_plot.update_cross_by_label(self.variable)# replor le spectre avec les lignes de reference
        ############# si pas d'acquisition, on plot le résultats déjà enegistré
        if self.acqui==1:
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7
            else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7
            self.time_fft_plot.update_data(self.variable.data_time[1:-2],20*numpy.log10(abs(data_plot[self.variable.slide_freq_ind,1:-2]/float(self.variable.ref))))
            self.spectro_plot.update_cross_by_label(self.variable)# replor le spectre avec les lignes de reference
#    


    def select_time_slide(self):
        time_search=float(self.time_slide_edit.text())
        time_list=abs(self.variable.data_time-numpy.array([time_search]*len(self.variable.data_time)))
        ind=numpy.where(time_list==min(time_list))[0]
        self.variable.slide_time_ind=ind[0]
        self.variable.slide_time=self.variable.data_time[self.variable.slide_time_ind]
        self.spectro_plot.update_cross_by_label(self.variable)# replor le spectre avec les lignes de reference
        ################## si pas d'acquisition, on plot les resulats enregistré
        if self.acqui==1:
            ###########################
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7
            else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7
            self.freq_fft_plot.update_data(self.variable.data_freq,20*numpy.log10(abs(data_plot[:,self.variable.slide_time_ind]/float(self.variable.ref))))
            self.spectro_plot.update_cross_by_label(self.variable)# replor le spectre avec les lignes de reference
#    
    def update_cross_spectro(self):
        time,time_ind,freq,freq_ind=self.spectro_plot.update_cross(self.variable)
        self.variable.slide_time=time
        self.variable.slide_time_ind=time_ind
        self.variable.slide_freq=freq
        self.variable.slide_freq_ind=freq_ind
        ###########################################
        self.freq_slide_edit.clear()
        self.freq_slide_edit.insert("{0:.1f}".format(self.variable.slide_freq))
        self.time_slide_edit.clear()
        self.time_slide_edit.insert("{0:.2f}".format(self.variable.slide_time))    
        ##########################################  
        if self.acqui==1:
             if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7
             else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7
             self.time_fft_plot.update_data(self.variable.data_time[1:-2],20*numpy.log10(abs(data_plot[self.variable.slide_freq_ind,1:-2]/float(self.variable.ref))))
             self.freq_fft_plot.update_data(self.variable.data_freq,20*numpy.log10(abs(data_plot[:,self.variable.slide_time_ind]/float(self.variable.ref))))
        

    
    def main(self):
        ########## intialisation des paramètre ########
        self.acqui=0
        self.define_data_freq_time()
        RATE=int(self.variable.factor_rate*self.variable.freq_max)
        BUFFER=self.variable.buffer_size
        if self.variable.window=='Hanning':
            weight=0.5-0.5*(numpy.cos(2*numpy.pi*numpy.arange(0,BUFFER)/float(BUFFER)))
        if self.variable.window=='Uniform':
            weight=numpy.ones((self.variable.buffer_size))
        taux_overlap=self.variable.covering
        nb_cover=int(round(1/(1-taux_overlap)))
        time_data=self.variable.data_time# ligne temporelle spectrogramme (prise en compte du recouvrement)
        freq_data=self.variable.data_freq# ligne freq spectrograme
        sensi=self.variable.sensi # on recupere la sensibilite
        ref=self.variable.ref # reference ppour le decibels
        ######### création des buffers et interpolation du signal pour 
        t1=self.signal_plot.vLine.value()
        t2=self.signal_plot.vLine2.value()
        if t1>t2:
            t=t1
            t1=t2
            t2=t
        ind1=numpy.where(self.variable.signal_time>t1)
        ind2=numpy.where(self.variable.signal_time<t2)
        ind=numpy.intersect1d(ind1,ind2)
        nb_point=int(round(numpy.size(self.variable.data_time)*(1-taux_overlap)*BUFFER))#int(self.variable.time_max*RATE)
        ################### reinitialise les slides si le temps est modifié
        self.time_slide_edit.clear()
        self.time_slide_edit.insert("{0:.1f}".format(0))
        self.select_time_slide() 
        self.freq_slide_edit.clear()
        self.freq_slide_edit.insert("{0:.1f}".format(0))
        self.select_freq_slide() 
        ###############♀ remet curseur 
        self.time_fft_plot.reset()
        self.freq_fft_plot.reset()
        ###########################################################        
        if len(ind)>0: # verfie que l'on n'a bien un signal à resampler
#            self.signal_bufferG.data=(1/float(sensi))*self.variable.signal[ind,0]
#            self.signal_bufferD.data=(1/float(sensi))*self.variable.signal[ind,1]
            self.signal_bufferG.data=(1/float(sensi))*numpy.interp(numpy.linspace(t1,t2,nb_point),self.variable.signal_time[ind],self.variable.signal[ind,0])
            self.signal_bufferD.data=(1/float(sensi))*numpy.interp(numpy.linspace(t1,t2,nb_point),self.variable.signal_time[ind],self.variable.signal[ind,1])
        ######### intialisation des ringbuffer et matrice vide #############
#            data_fft=1e-7*numpy.ones((BUFFER),'complex')
            self.spectro_plot.reset(self.variable)
            if self.variable.channel==0:
                self.spectro_plot.update_data(20*numpy.log10(abs(self.spectro_buffG.data).T),self.variable.db_min,self.variable.db_max)
            else:
                self.spectro_plot.update_data(20*numpy.log10(abs(self.spectro_buffD.data).T),self.variable.db_min,self.variable.db_max)
            for j in range(0,numpy.size(self.variable.data_time)-2):   
                ind_wind= numpy.arange(round(j*(1-taux_overlap)*BUFFER),round(j*(1-taux_overlap)*BUFFER)+BUFFER).astype(int)
                sig=numpy.zeros((numpy.size(ind_wind),2))
                try:
                    sig[0:numpy.size(ind_wind),0]=self.signal_bufferG.data[ind_wind]
                    sig[0:numpy.size(ind_wind),1]=self.signal_bufferD.data[ind_wind]
                except:
                    pass
                windowed_signalG=fenetre(weight,sig[0:BUFFER,0])
                windowed_signalD=fenetre(weight,sig[0:BUFFER,1])
#                windowed_signal=self.signal_buffer.data[ind_wind]
                data_fft=numpy.fft.fft(windowed_signalG,BUFFER)/float(BUFFER)
                data_fft[0]=0.5*data_fft[0]
                self.spectro_buffG.data[:,j+1]=2*numpy.array(data_fft[0:numpy.size(freq_data)])/float(self.variable.delta_freq)
                ######################################################################
                data_fft=numpy.fft.fft(windowed_signalD,BUFFER)/float(BUFFER)
                data_fft[0]=0.5*data_fft[0]
                self.spectro_buffD.data[:,j+1]=2*numpy.array(data_fft[0:numpy.size(freq_data)])/float(self.variable.delta_freq)
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec le logarithm
            else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec le logarithm
#################################################################################################################################♦
            self.spectro_plot.update_data(20*numpy.log10(data_plot.T/float(ref)),self.variable.db_min,self.variable.db_max)
            self.freq_fft_plot.update_data(freq_data,20*numpy.log10(data_plot[:,self.variable.slide_time_ind]/float(ref)))
            self.time_fft_plot.update_data(time_data[1:-2],20*numpy.log10(data_plot[self.variable.slide_freq_ind,1:-2]/float(ref)))
                ########################################################################
            app.processEvents()
            self.acqui=1
        #################################################################################♣
            self.start_button.setEnabled(True)
            self.export_button.setEnabled(True)
            self.listen_button.setEnabled(True)  
            self.wave_button.setEnabled(True)
                ##########################################################################
                
                
    def export_time_FFT(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_time_fft.txt',filter ='txt (*.txt *.)')
        fid=open(filename[0],'w')
        line='Amplitude vs Time    '+'Freq='+"{0:.1f}".format(self.variable.slide_freq)+'Hz \n'
        fid.write(line)
        fid.write('Time      Decibels\n')
        if self.variable.channel==0:
            data=20*numpy.log10(abs(self.spectro_buffG.data[self.variable.slide_freq_ind,:])/self.variable.ref)
        else:
            data=20*numpy.log10(abs(self.spectro_buffD.data[self.variable.slide_freq_ind,:]/self.variable.ref))
        for ii in range(0,numpy.size(self.variable.data_time)):
            fid.write("{0:.6f}".format(self.variable.data_time[ii])+';'+"{0:.3f}".format(data[ii])+'\n')
        fid.close()

    def export_freq_FFT(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_freq_fft.txt',filter ='txt (*.txt *.)')
        fid=open(filename[0],'w')
        line='Amplitude vs Freq     '+'Time='+"{0:.3f}".format(self.variable.slide_time)+'s \n'
        fid.write(line)
        fid.write('Freq      Decibels\n')
        if self.variable.channel==0:
            data=20*numpy.log10(abs(self.spectro_buffG.data[:,self.variable.slide_time_ind])/self.variable.ref)
        else:
            data=20*numpy.log10(abs(self.spectro_buffD.data[:,self.variable.slide_time_ind])/self.variable.ref)
        for ii in range(0,numpy.size(self.variable.data_time)):
            fid.write("{0:.3f}".format(self.variable.data_freq[ii])+';'+"{0:.3f}".format(data[ii])+'\n')        
        fid.close()

    
    def export_spectro_FFT(self,data):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_spectro_fft.txt',filter ='txt (*.txt *.)')
        fid=open(filename[0],'w')
        line='Freq (Freq values:line 1) vs Time (Time values:row 1) in dB/Hz\n'
        fid.write(line)
        if self.variable.channel==0:
            write_spectro(fid,20*numpy.log10(abs(self.spectro_buffG.data)/self.variable.ref),self.variable.data_freq,self.variable.data_time)
        else:
             write_spectro(fid,20*numpy.log10(abs(self.spectro_buffD.data)/self.variable.ref),self.variable.data_freq,self.variable.data_time)
        fid.close()        
        
    def listen_sig(self):
        self.listen_button.setEnabled(False)
        app.processEvents()
        p=pyaudio.PyAudio()
        BUFFER=self.variable.buffer_size
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        stream = p.open(format=pyaudio.paFloat32,channels= 2,rate=RATE,input=False,output=True,frames_per_buffer=BUFFER)
        data=numpy.zeros((numpy.size(self.signal_bufferG.data),2))
        data[:,0]=self.signal_bufferG.data
        data[:,1]=self.signal_bufferD.data
        stream.write(data.astype(numpy.float32).tostring())
        stream.close()
        p.terminate()
#        QtCore.QThread.sleep(self.variable.time_max)
        self.listen_button.setEnabled(True)
        app.processEvents()
                 
    def save_wave(self):
        data=numpy.zeros((numpy.size(self.signal_bufferG.data),2))        
        data[:,0]=numpy.multiply(self.signal_bufferG.data,32767).astype(numpy.int16)
        data[:,1]=numpy.multiply(self.signal_bufferD.data,32767).astype(numpy.int16)
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','checktone.wav',filter ='wav (*.wav *.)')
        wave_file = wave.open(filename[0], 'w')
        wave_file.setparams((2, 2, RATE, 0, 'NONE', 'not compressed'))  
        for ii in range(numpy.size(data[:,0])):
            signal= wave.struct.pack('<hh', int(data[ii,0]),int(data[ii,1]))
            wave_file.writeframesraw( signal )
#        scipy.io.wavfile.write(filename, RATE, data2)
        wave_file.close()
        
        
        
if __name__=='__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    splash=splashScreen()
    splash.show_splash()  
    app.processEvents()
    QtCore.QThread.sleep(2)
    splash.close_splash()
    gui = spectro()
    gui.show()
    app.exec_()# -*- coding: utf-8 -*-  
    me = os.getpid()
    kill_proc_tree(me)
    
